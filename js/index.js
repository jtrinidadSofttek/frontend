
var Negocio = function Negocio(Id, Nombre, Direccion, Email) {
    this.Id = Id;
    this.Nombre = Nombre;
    this.Direccion = Direccion;
    this.Email = Email;
}

$(function () {
    // Variables globales
    var form = $("#form");
    var idNegocio = $("#idNegocio")
    var nombre = $("#nombre");
    var email = $("#email");
    var direccion = $("#direccion")
    var list = $("#tbListaNegocios");
    var btnAdd = $("#btnAdd");
    var TituloEditar = $("TituloEditar");
    var i = 0;

    //inicializacion del display
    cargarDatos();
    eventos();

    function eventos() {

        document.addEventListener('click', function (e) {
            // Fixed

            if (e.target.tagName.toLowerCase() === 'button') {

                if (e.target.id === 'btnAdd') {
                    console.log(e.target.id);
                    AddItem();
                    M.toast({ html: 'El objeto ha sido creado!' })
                }
                if (e.target.name === 'btnEdit') {
                    EditItem(e.target.id);
                }
                if (e.target.name === 'update') {
                    SaveItem(e.target.id);
                    M.toast({ html: 'El objeto ha sido actualizado!' })
                }
                if (e.target.name === 'btnRemove') {
                    RemoveDatos(e.target.id);
                    M.toast({ html: 'El objeto ha sido eliminado!' })
                }
            }
        })
    }

    function AddItem() {
        var negocio = new Negocio(0, nombre.val(), direccion.val(), email.val());
        console.log(negocio);
        PostDatos(negocio);
        form[0].reset();
    }


    function GetNegocios(negocios) {
        console.log(negocios);
        for (var i in negocios) {
            $("#tbListaNegocios").append(
                "<tr class='table-secondary'>" +
                "<td>" + negocios[i].Nombre + "</td>" +
                "<td>" + negocios[i].Direccion + "</td>" +
                "<td>" + negocios[i].Email + "</td>" +
                "<td> <button type='button'  id=" + negocios[i].Id+ " name='btnRemove'  class='btn btn-danger' > Eliminar" + "</button>" +
                "<button type='button' data-id=" + i + " id=" + negocios[i].Id  + " name='btnEdit' class='btn btn-warning'> Editar" + "</button> </td>" +
                "<td>  <a  href='empleado.html'  id="+i+" name='btnEmpleado'  class='btn btn-info' > Ver" + "</a>" + "</td>"+
                "</tr>"
            );
        }

    }

    function GetNegocio(negocio){
        var negocio = new Negocio(negocio.Id, negocio.Nombre, negocio.Direccion, negocio.Email);
        nombre.val(negocio.Nombre);
        direccion.val(negocio.Direccion);
        email.val(negocio.Email);
    }


    function EditItem(idData) {
        CargarDato(idData);
        id = idData;
        btnAdd.attr("id", "actualizar");
        $("#actualizar").attr("name", "update");
        $("#actualizar").text("Guardar");
        $("#TituloEditar").text("Editar Negocio");
        btnAdd.attr("id", id);

    }

    function SaveItem(idData) {
        console.log(idData);
        var newNegocio = new Negocio(idData, nombre.val(), direccion.val(), email.val());
        PutDatos(newNegocio, idData);
        $("[name='update']").attr("id", "btnAdd");
        btnAdd.text("Crear");
        btnAdd.attr("name", "");
        $("#TituloEditar").text("Crear Negocio");
        form[0].reset();
        
    }

    function cargarDatos() { 
        $.ajax({
        method: "GET",
        url: "https://localhost:44395/api/Negocio/",
    })
        .done(function (result) {
            GetNegocios(result);
        })
        .fail(function () {
            alert("error");

        })
        .always(function () {
            console.log("complete");
        });
    }

    function PostDatos(negocio) {
        $.ajax({
            method: "POST",
            url: "https://localhost:44395/api/negocio",
            data: { Nombre: negocio.Nombre, Email: negocio.Email, Direccion: negocio.Direccion},
            dataType: "json"
        })
            .done(function (data) {
                console.log(data)
                location.reload(true);
            })
            .fail(function (data, error) {
                console.log(error);
                console.log(data)
            })
            .always(function () {
                console.log("complete");
            });
    }

    function RemoveDatos(id) {
        $.ajax({
            method: "DELETE",
            url: `https://localhost:44395/api/negocio/${id}`,

        })
            .done(function (id) {
                console.log(id)
                location.reload(true);
            })
            .fail(function (error, id) {
                console.log(error);
                console.log(id);
            })
            .always(function () {
                console.log("complete");
            });
    }

    function CargarDato(id) {
        $.ajax({
            method: "GET",
            url: `https://localhost:44395/api/negocio/${id}`,

        })
            .done(function (data) {
                GetNegocio(data);
               // location.reload(true);
            })
            .fail(function (error, id) {
                console.log(error);
                console.log(id);
            })
            .always(function () {
                console.log("complete");
            });
    }

    function PutDatos(negocio, id) {
        $.ajax({
            method: "PUT",
            url: `https://localhost:44395/api/negocio/${id}`,
            data: {Id: id, Nombre: negocio.Nombre, Email: negocio.Email, Direccion: negocio.Direccion},
            dataType: "json"
        })
            .done(function (data) {
                console.log(data)
                location.reload(true);
            })
            .fail(function (data, error) {
                console.log(error);
                console.log(data)
            })
            .always(function () {
                console.log("complete");
            });
    }

});

