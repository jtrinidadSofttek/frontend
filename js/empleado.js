
var Empleado = function Empleado(nombre, dni, sueldo, negocio) {
    this.nombre = nombre;
    this.dni = dni;
    this.sueldo = sueldo;
    this.negocio = negocio;
}

$(function () { 
    //declaro variables globales
    var form = $("#formEmpleado");
    var nombre = $("#nombre");
    var dni = $("#dni");
    var sueldo = $("#sueldo")
    var list = $("#tbListaEmpleados");
    var btnAdd = $("#btnAddEmpleado");
    var TituloEditar = $("EmpleadoEditar");
    var i = 0;

    var opcionesNegocio = $("#opcionesNegocio");

    GetEmpleadosAJAX();
    GetNegocio() 
    eventos();

    function eventos() {

        document.addEventListener('click', function (e) {

            if (e.target.tagName.toLowerCase() === 'button') {

                if (e.target.id === 'btnAddEmpleado') {
                    console.log(e.target.id);
                    AddEmpleado();
                    M.toast({ html: 'El objeto ha sido creado!' })
                }
                if (e.target.name === 'btnEdit') {
                    EditItem(e.target.id);
                }
                if (e.target.name === 'update') {
                    SaveItem(e.target.id);
                    M.toast({ html: 'El objeto ha sido actualizado!' })
                }
                if (e.target.name === 'btnRemove') {
                    RemoveItem(e.target.id);
                    M.toast({ html: 'El objeto ha sido eliminado!' })
                }
            }

        })
    }
    function AddEmpleado() {
        var empleado = JSON.stringify(new Empleado(nombre.val(), dni.val(), sueldo.val(), opcionesNegocio.val()));
        PostDatos(empleado);
        form[0].reset();
        console.log(empleado);
        location.reload();
    }

    function GetEmpleados(empleados) {
        console.log(empleados);
        for (var i in empleados) {
            $("#tbListaEmpleados").append(
                "<tr>" +
                "<td>" + empleados[i].Nombre + "</td>" +
                "<td>" + empleados[i].Dni + "</td>" +
                "<td>" + empleados[i].Sueldo + "</td>" +
                "<td>" + empleados[i].Id_Negocio + "</td>" +
                "<td> <button type='button'  id=" + i + " name='btnRemoveEmpleado' class='waves-effect waves-light btn disabled' > Eliminar" + "</button>" +
                "<button type='button' data-id=" + i + " id=" + i + " name='btnEditEmpleado' class='waves-effect waves-light btn disabled'> Editar" + "</button> </td>" +
                "</tr>"
            );
        }

    }

    function GetEmpleadosAJAX() { 
        $.ajax({
        method: "GET",
        url: "https://localhost:44395/api/empleado",
    })
        .done(function (result) {
            GetEmpleados(result);
        })
        .fail(function () {
            alert("error");
        })
        .always(function () {
            console.log("complete");
        });
    }

    function GetNegocio() { 
        $.ajax({
        method: "GET",
        url: "https://prueba01-63eb9.firebaseio.com/JC/Negocios.json",
    })
        .done(function (result) {
            for(var i in result){
                opcionesNegocio.append(new Option(result[i].nombre, result[i].nombre))                
                }
        })
        .fail(function () {
            alert("error");
        })
        .always(function () {
            console.log("complete");
        });
    }

    function PostDatos(empleado) {
        $.ajax({
            method: "POST",
            url: "https://prueba01-63eb9.firebaseio.com/JC/Personas.json",
            data: empleado,
            contentType: "application/json"
        })
            .done(function (data) {
                console.log(data)
            })
            .fail(function (error) {
                console.log(error);
            })
            .always(function () {
                console.log("complete");
            });
    }


});
